## Lab IV: Getting Ready for Sorting
Here, we want to create an object called ```DLine``` using which one can practice sorting methods.  

The class ```DLine``` allows creation of an object which is a dashed line with a specific length.
This class has one single field called ```length``` that indicates the number of dashes to be used for creating the line.

### Objectives:
* The primary motivation is to have some fun with sorting a collection of lines with respect to their length, instead of sorting an array of numbers.
* Using customized objects could discourage students to copy snippets of codes for sorting instead of implementing them by themselves.  

### Description
The details of this project can be found at [class page](https://www.azim-a.com/teaching/data-structures-2720).

#### Author
* *Azim Ahmadzadeh* - [webpage](https://www.azim-a.com/)
#### Course
* *Data Structures - 2720* - Fall 2018
#### School
* [Computer Science Department](https://www.cs.gsu.edu/) - Georgia State University
#### License
This project is licensed under the GNU General Public License - see the [GPL LICENSE](http://www.gnu.org/licenses/gpl.html) for details.



